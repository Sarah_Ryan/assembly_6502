This repo will hold multiple small applications made in 6502 assembly.
Each text file will contain the entire source code for an application.

To run the applications, copy and paste the contents of a text file into the input box found on http://www.6502asm.com/. Then hit compile, run and the output will be shown on the small screen next to it.

Currently in the repo:

Colours.txt - Small app that colours the screen evenly with 4 colour layers. Creating this helped me to teach myself the basics of 6502, how to use a high and low bit and how to use dcb's to store data and ensure it's easily editable.