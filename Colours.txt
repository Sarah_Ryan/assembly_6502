;init
ldy #0
lda #0
sta $0
lda #2
sta $1

Loop:
sta ($0), y
iny
Bne Loop
inc $1
ldx $1
lda Colours, x
jmp Loop

Colours:
dcb 0, 0, 10, 3, 7, 6